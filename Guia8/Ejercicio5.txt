// Para hacer el algoritmo, realizaría dos sorts en O(n log n)
// Complejidad total: O(n log n)

proc ordenarPorFrecuencia(Array<int> arr): Array<int> {
  mergeSort(arr); // O(n log n)
  
  int val = arr[0], cant = 1;
  Vector<tupla<int,int>> arrTuplificado = vectorVacio(); // Se ordena por primera variable de mayor a menor, y en caso de empate, por segunda variable de menor a mayor

  for (int i = 1; i < arr.length; i++) { // O(n) total del loop
    if (val != arr[i]) { // O(1)
      arrTuplificado.agregarAtras(new tupla<int, int>(cant,val)); // O(1) amortizado
      val = arr[i]; cant = 1; // O(1)
    } else {
      cant = cant + 1; // O(1)
    }
  }

  arrTuplificado.agregarAtras(new tupla<int, int>(cant,val)); // O(1)
  mergeSort(arrTuplificado);

  Array<int> ans = new Array<int> (arr.length); // O(n)
  int counter = 0; // O(1)

  for (int i = 0; i < arrTuplificado.longitud(); i++) { // O(n), ya que la suma de cantidades en todo el array es n
    while (arrTuplificado[i][0] > 0) { // O(arrTuplificado[i][0])
      ans[counter] = arrTuplificado[i][1]; // O(1)
      arrTuplificado[i][0] = arrTuplificado[i][0] - 1; // O(1)
    }
  }

  return ans; // O(1)
}

